import { mount, shallowMount } from '@vue/test-utils'
import PicrossEditor from '../../src/components/PicrossEditor.vue'

describe('PicrossEditor.vue', () => {
  it('renders without crashing', () => {
    const wrapper = mount(PicrossEditor)
    expect(wrapper).toBeDefined()
  })
  it('renders with props', () => {
    const wrapper = mount(PicrossEditor, {
      propsData: {
        numberRows: 10,
        numberCells: 10
      }
    })
    expect(wrapper.vm.numberRows).toBe(10)
    expect(wrapper.vm.cellsValues).toBeDefined()
    expect(wrapper.vm.cellsValues.length).toBe(10)
    const cellsValuesExpected = []
    for (let i = 0; i < 10; i++) {
      const currentRowCellsValues = new Array(10).fill(false)
      cellsValuesExpected.push(currentRowCellsValues)
    }
    expect(wrapper.vm.cellsValues).toEqual(cellsValuesExpected)
    expect(wrapper).toMatchSnapshot()
  })
  it('emit update-picross-data', async () => {
    const onUpdatePicrossData = jest.fn()
    const wrapper = shallowMount(PicrossEditor, {
      propsData: { numberRows: 10, numberCells: 10 },
      listeners:{
        updatePicrossData: onUpdatePicrossData
      }
    })
    expect(wrapper).toBeDefined()
    const cell = wrapper.find('table tr:nth-child(2) td:nth-child(3)')
    cell.trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper).toMatchSnapshot()
    expect(onUpdatePicrossData).toBeCalled()
    expect(onUpdatePicrossData.mock.calls.length).toBe(1)
    const cellsValuesExpected =[]
    for (let i =0; i< 10; i++) {
      const currentRowCellsValues = new Array(10).fill(false)
      cellsValuesExpected.push(currentRowCellsValues)
    }
    cellsValuesExpected[1][2] = true
    expect(onUpdatePicrossData.mock.calls[0][0]).toEqual(cellsValuesExpected)
  })
})
