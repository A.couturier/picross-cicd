import Play from '@/views/Play.vue'
import { mount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'

const localVue = createLocalVue()
localVue.use(VueRouter)
const router = new VueRouter()

const picrossData = [{
  title: '4 x 4',
  data: { rows: [[1], [2], [2], [1]], columns: [[1], [2], [2], [1]] },
  size: { rows: 4, columns: 4 }
}]
describe('Player.vue', () => {
  it('renders without crashing', async () => {
    fetch.mockResponseOnce(JSON.stringify(picrossData))
    const wrapper = mount(Play, {
      localVue,
      router
    })
    expect(wrapper).toBeDefined()
    await wrapper.vm.$nextTick()
    await wrapper.vm.$nextTick()
    await wrapper.vm.$nextTick()
    expect(wrapper).toMatchSnapshot()
  })
})
