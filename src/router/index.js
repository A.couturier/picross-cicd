import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Creation from '../views/Creation.vue'
// import Play from '../views/Play.vue'
import Apropos from '../views/Apropos.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/Creation',
    name: 'Creation',
    component: Creation
  },
  {
    path: '/Apropos',
    name: 'Apropos',
    component: Apropos
  },
  {
    path: '/Play',
    name: 'Play',
    component: () => import(/* webpackChunkName: "Play" */ '../views/Play.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
